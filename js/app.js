var auth_token='i2vSufO9jXrxEDu673nD1EV3yx8dWcJocqRTUyiE';
var skip_limit=25;
var school_skip_count=0;
var news_skip_count=0;
var app=angular.module('finder', ['ionic','ionic.rating','ngCordova','ionMDRipple'])
app.run(function($ionicPlatform,$cordovaGoogleAnalytics,$state,$ionicHistory,location) {
  $ionicPlatform.ready(function() {
    ionic.Platform.fullScreen();
    if(window.Connection) {
      if(navigator.connection.type == Connection.NONE) {
        var message = "Sorry, no Internet connectivity detected. Please reconnect and try again...";
        var title = "No Internet Connection";
        navigator.notification.alert(message,alertDismissed, title, 'OK');
        function alertDismissed() {
          ionic.Platform.exitApp();
        }
      }
    }
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(false);
      cordova.plugins.Keyboard.disableScroll(false);
    }

    CheckGPS.check(function win(){
      location.set();
    },
    function fail(){
        var message = "School Finder needs access to your location. Please go to your settings and enable Location";
        var title = "Location Services disabled";
        var buttonLabels = ["GoTo Settings","Cancel"];
        navigator.notification.confirm(message, confirmCallback, title, buttonLabels);
        function confirmCallback(buttonIndex) {
          if(buttonIndex==1) {
            if(typeof cordova.plugins.settings.openSetting != undefined){
              if(ionic.Platform.isIOS()){
                cordova.plugins.settings.open()
              }else{
                cordova.plugins.settings.open("location_source",function(){}, function(){});
              }
              $window.location.reload(true);
            }
          }
        }
    });
    if(typeof analytics !== 'undefined'){
      $cordovaGoogleAnalytics.debugMode();
      $cordovaGoogleAnalytics.startTrackerWithId('UA-91419216-1');
      $cordovaGoogleAnalytics.trackView('APP first screen');
    }
  });
  $ionicPlatform.onHardwareBackButton(function (event) {
    if(($state.current.name=="tab.find")||($state.current.name=="tab.school")||($state.current.name=="tab.rating")){
        $state.go('home');
    }else if($state.current.name=="home"){
        ionic.Platform.exitApp();
    }else{
        $ionicHistory.goBack();
    }
  });
})

app.config(function($stateProvider, $urlRouterProvider,$ionicConfigProvider) {
$ionicConfigProvider.tabs.position('bottom'); // other values: top
$ionicConfigProvider.backButton.previousTitleText(false);
$ionicConfigProvider.views.transition('none');
$stateProvider
.state('home', {
  cache:false,
  url: '/home',
  templateUrl: 'templates/home.html',
  controller: 'HomeCtrl'
})
.state('tab', {
  url: '/tab',
  abstract: true,
  templateUrl: 'templates/tabs.html'
})
.state('tab.find', {
  cache:false,
  url: '/find',
  views: {
    'tab-find': {
      templateUrl: 'templates/tab-find.html',
      controller: 'FindCtrl'
    }
  }
})
.state('tab.find-list', {
  url: '/finds',
  params: {obj:null},
  views: {
    'tab-find': {
      templateUrl: 'templates/find-list.html',
      controller: 'FindListCtrl'
    }
  }
})
.state('tab.find-list-map', {
  url: '/find-list-map/:id',
  params: {obj:null},
  views: {
    'tab-find': {
      templateUrl: 'templates/find-list-map.html',
      controller: 'FindListMapCtrl'
    }
  }
})
.state('tab.find-detail', {
  url: '/finds/:id',
  params: {obj:null},
  views: {
    'tab-find': {
      templateUrl: 'templates/find-detail.html',
      controller: 'FindDetailCtrl'
    }
  }
})
.state('tab.school', {
  url: '/school',
  views: {
    'tab-school': {
      templateUrl: 'templates/tab-school.html',
      controller: 'SchoolCtrl'
    }
  }
})
.state('tab.school-detail', {
  url: '/schools',
  params: {obj:null},
  views: {
    'tab-school': {
      templateUrl: 'templates/school-detail.html',
      controller: 'SchoolDetailCtrl'
    }
  }
})
.state('tab.rating', {
  url: '/rating',
  views: {
    'tab-rating': {
      templateUrl: 'templates/tab-rating.html',
      controller: 'RatingCtrl'
    }
  }
});
$urlRouterProvider.otherwise('/home');
});

app.factory('api', function() {
  return {
// url: 'http://schoolfinder/',
url:'http://schoolfinder.acecommunicate.com/',
};
});
// app.config(function($provide) {
//   return $provide.decorator('$exceptionHandler', function($delegate) {
//     return function(exception, cause) {
//       var initInjector = angular.injector(['ng']);
//       $http = initInjector.get('$http');
//       var params = {
//         message: exception.message,
//         cause: cause,
//         stack: exception.stack
//       };
//       $http.post(window['Settings'].API_HOST + '/js_errors.json', params)
//       $delegate(exception, cause);
//     };
//   });
// });
