app.service('area', function($cordovaDialogs) {
  var setloc = function(data) {
    window.localStorage.arealoc = JSON.stringify(data);
  };
  var getloc = function(){
    return JSON.parse(window.localStorage.arealoc || 'null');
  };
  return {
    getloc: getloc,
    setloc: setloc
  };
});

app.factory('location', function($cordovaDialogs,area) {
  var options = {enableHighAccuracy: false ,timeout :10000};
  return {
    set: function(){
      return navigator.geolocation.getCurrentPosition(function(pos) {
        area.setloc(new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude));
      },function(error) {
        console.log(error);
        // $cordovaDialogs.alert(""+JSON.stringify(error), 'Location Disabled', 'OK').then(function() {});
      }, options);
    }
  }
});

app.config(['$httpProvider', function ($httpProvider) {
  $httpProvider.defaults.useXDomain = true;
  $httpProvider.defaults.headers.common = 'Content-Type: application/json';
  delete $httpProvider.defaults.headers.common['X-Requested-With'];
}]);

app.factory('ajax', function($http,$q,$window) {
  return {
    post: function(url,obj) {
      var deferred = $q.defer();
      $http.post(url, obj)
      .success(function(data) {
        deferred.resolve(data);
      }).error(function(msg, code) {
        deferred.reject(code);
      });
      return deferred.promise;
    }
  };
});

app.service('UserService', function() {
// For the purpose of this example I will store user data on ionic local storage but you should save it on a database
var setUser = function(user_data) {
  window.localStorage.starter_facebook_user = JSON.stringify(user_data);
};
var getUser = function(){
  return JSON.parse(window.localStorage.starter_facebook_user || '{}');
};
return {
  getUser: getUser,
  setUser: setUser
};
});

app.factory('browser', function($cordovaInAppBrowser,$rootScope) {
  return {
    open: function(obj) {
      var options = {
        location: 'no',
        clearcache: 'yes',
        toolbar: 'yes',
        closebuttoncaption: 'Back',
        transitionstyle:'crossdissolve'
      };
      $cordovaInAppBrowser.open(obj, '_blank', options)
      .then(function(event) {
        console.log("success");
      })
      .catch(function(event) {
        console.log("failure");
      });
    }
  }
});

app.factory('ClockSrv', function($interval){
  var clock = null;
  var service = {
    startClock: function(fn){
      if(clock === null){
        clock = $interval(fn, 10000);
      }
    },
    stopClock: function(){
      if(clock !== null){
        $interval.cancel(clock);
        clock = null;
      }
    }
  };
  return service;
});

app.directive('ionAffix', ['$ionicPosition', '$compile', function ($ionicPosition, $compile) {

// keeping the Ionic specific stuff separated so that they can be changed and used within an other context

// see https://api.jquery.com/closest/ and http://ionicframework.com/docs/api/utility/ionic.DomUtil/
function getParentWithClass(elementSelector, parentClass) {
  return angular.element(ionic.DomUtil.getParentWithClass(elementSelector[0], parentClass));
}

// see http://underscorejs.org/#throttle
function throttle(theFunction) {
  return ionic.Utils.throttle(theFunction);
}

// see http://www.paulirish.com/2011/requestanimationframe-for-smart-animating/
// see http://ionicframework.com/docs/api/utility/ionic.DomUtil/
function requestAnimationFrame(callback) {
  return ionic.requestAnimationFrame(callback);
}

// see https://api.jquery.com/offset/
// see http://ionicframework.com/docs/api/service/$ionicPosition/
function offset(elementSelector) {
  return $ionicPosition.offset(elementSelector);
}

// see https://api.jquery.com/position/
// see http://ionicframework.com/docs/api/service/$ionicPosition/
function position(elementSelector) {
  return $ionicPosition.position(elementSelector);
}

function applyTransform(element, transformString) {
// do not apply the transformation if it is already applied
if (element.style[ionic.CSS.TRANSFORM] == transformString) {
}
else {
  element.style[ionic.CSS.TRANSFORM] = transformString;
}
}

function translateUp(element, dy, executeImmediately) {
  var translateDyPixelsUp = dy == 0 ? 'translate3d(0px, 0px, 0px)' : 'translate3d(0px, -' + dy + 'px, 0px)';
// if immediate execution is requested, then just execute immediately
// if not, execute in the animation frame.
if (executeImmediately) {
  applyTransform(element, translateDyPixelsUp);
}
else {
// see http://www.paulirish.com/2011/requestanimationframe-for-smart-animating/
// see http://ionicframework.com/docs/api/utility/ionic.DomUtil/
requestAnimationFrame(function () {
  applyTransform(element, translateDyPixelsUp);
});
}
}

var CALCULATION_THROTTLE_MS = 500;

return {
// only allow adding this directive to elements as an attribute
restrict: 'A',
// we need $ionicScroll for adding the clone of affix element to the scroll container
// $ionicScroll.element gives us that
require: '^$ionicScroll',
link: function ($scope, $element, $attr, $ionicScroll) {
// get the affix's container. element will be affix for that container.
// affix's container will be matched by "affix-within-parent-with-class" attribute.
// if it is not provided, parent element will be assumed as the container
var $container;
if ($attr.affixWithinParentWithClass) {
  $container = getParentWithClass($element, $attr.affixWithinParentWithClass);
  if (!$container) {
    $container = $element.parent();
  }
}
else {
  $container = $element.parent();
}

var scrollMin = 0;
var scrollMax = 0;
var scrollTransition = 0;
// calculate the scroll limits for the affix element and the affix's container
var calculateScrollLimits = function (scrollTop) {
  var containerPosition = position($container);
  var elementOffset = offset($element);

  var containerTop = containerPosition.top;
  var containerHeight = containerPosition.height;

  var affixHeight = elementOffset.height;

  scrollMin = scrollTop + containerTop;
  scrollMax = scrollMin + containerHeight;
  scrollTransition = scrollMax - affixHeight;
};
// throttled version of the same calculation
var throttledCalculateScrollLimits = throttle(
  calculateScrollLimits,
  CALCULATION_THROTTLE_MS,
  {trailing: false}
  );

var affixClone = null;

// creates the affix clone and adds it to DOM. by default it is put to top
var createAffixClone = function () {
  var clone = $element.clone().css({
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0
  });

// if directive is given an additional CSS class to apply to the clone, then apply it
if ($attr.affixClass) {
  clone.addClass($attr.affixClass);
}

// remove the directive matching attribute from the clone, so that an affix is not created for the clone as well.
clone.removeAttr('ion-affix').removeAttr('data-ion-affix').removeAttr('x-ion-affix');

angular.element($ionicScroll.element).append(clone);

// compile the clone so that anything in it is in Angular lifecycle.
$compile(clone)($scope);

return clone;
};

// removes the affix clone from DOM. also deletes the reference to it in the memory.
var removeAffixClone = function () {
  if (affixClone)
    affixClone.remove();
  affixClone = null;
};

$scope.$on("$destroy", function () {
// 2 important things on destroy:
// remove the clone
// unbind the scroll listener
// see https://github.com/aliok/ion-affix/issues/1
removeAffixClone();
angular.element($ionicScroll.element).off('scroll');
});


angular.element($ionicScroll.element).on('scroll', function (event) {
  var scrollTop = (event.detail || event.originalEvent && event.originalEvent.detail).scrollTop;
// when scroll to top, we should always execute the immediate calculation.
// this is because of some weird problem which is hard to describe.
// if you want to experiment, always use the throttled one and just click on the page
// you will see all affix elements stacked on top
if (scrollTop == 0) {
  calculateScrollLimits(scrollTop);
}
else {
  throttledCalculateScrollLimits(scrollTop);
}

// when we scrolled to the container, create the clone of element and place it on top
if (scrollTop >= scrollMin && scrollTop <= scrollMax) {

// we need to track if we created the clone just now
// that is important since normally we apply the transforms in the animation frame
// but, we need to apply the transform immediately when we add the element for the first time. otherwise it is too late!
var cloneCreatedJustNow = false;
if (!affixClone) {
  affixClone = createAffixClone();
  cloneCreatedJustNow = true;
}

// if we're reaching towards the end of the container, apply some nice translation to move up/down the clone
// but if we're reached already to the container and we're far away than the end, move clone to top
if (scrollTop > scrollTransition) {
  translateUp(affixClone[0], Math.floor(scrollTop - scrollTransition), cloneCreatedJustNow);
} else {
  translateUp(affixClone[0], 0, cloneCreatedJustNow);
}
} else {
  removeAffixClone();
}
});
}
}
}]);

app.factory('TwitterLib', function ($rootScope, $q, $window, $http, myAppConfig, $state) {

// GLOBAL VARS

var runningInCordova = false;
var loginWindow;


// Construct the callback URL fro when running in browser
var index = document.location.href.indexOf('index.html');
var callbackURL = document.location.href.substring(0, index) + 'oauthcallback.html';

var oauth;

// YOUR Twitter CONSUMER_KEY, Twitter CONSUMER_SECRET
var options;

options = angular.extend({}, myAppConfig.oauthSettings);
options = angular.extend(options, {
  callbackUrl: callbackURL
});

// YOU have to replace it on one more Place
// This key is used for storing Information related
// var twitterKey = 'PU47tNh833f96TZ1OlrspA';

var twitterKey = 'TWITTERKEY';

// used to check if we are running in phonegap/cordova
$window.document.addEventListener("deviceready", function () {
  runningInCordova = true;

  callbackURL = myAppConfig.oauthSettings.callbackUrl;
  options.callbackUrl = callbackURL;
}, false);

function byteArrayToString(byteArray) {
  var string = '', l = byteArray.length, i;
  for (i = 0; i < l; i++) {
    string += String.fromCharCode(byteArray[i]);
  }
  return string;
}

var Twitter = {
  init: function () {

// alert('TEST init!');

var deferredLogin = $q.defer();
//the event handler for processing load events for the oauth
//process
//@param event
var doLoadstart = function (event) {
// alert('TEST doLoadstart');
console.log("in doLoadstart " + event.url);
var url = event.url;
Twitter.inAppBrowserLoadHandler(url, deferredLogin);
};
//
// the event handler for processing exit events for the oauth
// process
//@param event
var doExit = function (event) {
// Handle the situation where the user closes the login window manually
// before completing the login process
console.log(JSON.stringify(event));
deferredLogin.reject({error: 'user_cancelled',
  error_description: 'User cancelled login process',
  error_reason: "user_cancelled"
});
};

var openAuthoriseWindow = function (_url) {

// alert('TEST openAuthoriseWindow');

loginWindow = $window.open(_url, '_blank', 'location=yes');

if (runningInCordova) {
  loginWindow.addEventListener('loadstart', doLoadstart);

} else {
// this saves the promise value in the window when running in the browser
window.deferredLogin = deferredLogin;
}
};

var failureHandler = function () {
  console.log("ERROR: " + JSON.stringify(error));
  deferredLogin.reject({error: 'user_cancelled', error_description: error });
};

// Apps storedAccessData , Apps Data in Raw format
var storedAccessData, rawData = localStorage.getItem(twitterKey);
// here we are going to check whether the data about user is already with us.
if (localStorage.getItem(twitterKey) !== null) {
// alert('TEST verified');
Twitter.verify(deferredLogin);
$state.go('tab.find');

} else {
// we have no data for save user
// alert('TEST not verified');
oauth = OAuth(options);
oauth.fetchRequestToken(openAuthoriseWindow, failureHandler);
}

return deferredLogin.promise;
},
//
//When inAppBrowser's URL changes we will track it here.
//We will also be acknowledged was the request is a successful or unsuccessful
//@param _url url received from the event
//@param _deferred promise associated with login process
inAppBrowserLoadHandler: function (_url, _deferred) {
// alert('TEST inAppBrowserLoadHandler');

// this gets the promise value from the window when running in the browser
_deferred = _deferred || window.deferredLogin;

//
//@param _args
var successHandler = function (_args) {

// alert('successHandler: ' + _args);
console.log(_args);
// Saving token of access in Local_Storage
var accessData = {};
accessData.accessTokenKey = oauth.getAccessToken()[0];
accessData.accessTokenSecret = oauth.getAccessToken()[1];
// Configuring Apps LOCAL_STORAGE
console.log("TWITTER: Storing token key/secret in localStorage");
$rootScope.accessData = accessData;
$window.localStorage.setItem(twitterKey, JSON.stringify(accessData));
Twitter.verify(_deferred);

};
//
//@param _args
var failureHandler = function (_args) {
  console.log("ERROR - oauth_verifier: " + JSON.stringify(_args));
  _deferred.reject({error: 'user_cancelled', error_description: _args })
};

console.log("callbackURL " + callbackURL);

if(_url.indexOf('oauth_verifier') >= 0) {
// if (_url.indexOf(callbackURL + "/?") >= 0) {

// Parse the returned URL
var params, verifier = '';
params = _url.substr(_url.indexOf('?') + 1);

params = params.split('&');
for (var i = 0; i < params.length; i++) {
  var y = params[i].split('=');
  if (y[0] === 'oauth_verifier') {
    verifier = y[1];
  }
}
oauth.setVerifier(verifier);
oauth.fetchAccessToken(successHandler, failureHandler);

loginWindow.close();
} else {
// Just Empty
}
},
//
//this will verify the user and store the credentials if needed
verify: function (_deferred) {

// alert('TEST verify');

var deferred = _deferred || $q.defer();
var storedAccessData, rawData = localStorage.getItem(twitterKey);
storedAccessData = JSON.parse(rawData);

// javascript OAuth will care of else for app we need to send only the options
oauth = oauth || OAuth(options);

oauth.setAccessToken([storedAccessData.accessTokenKey, storedAccessData.accessTokenSecret]);

oauth.get('https://api.twitter.com/1.1/account/verify_credentials.json?skip_status=true&&include_email=true',
  function (data) {
    $rootScope.userData = JSON.parse(data.text);
// $rootScope.dataType = typeof userData;
// $rootScope.name = userData.name;
// $rootScope.data = userData;
console.log("in verify resolved " + data.text);
deferred.resolve(JSON.parse(data.text));
// $state.go('tab.dash');
}, function (_error) {
  console.log("in verify reject " + _error);
  deferred.reject(JSON.parse(_error));
}
);
return deferred.promise;
},
//this will verify the user and send a tweet
//@param _message
tweet: function (_message, _media) {

  var deferred = $q.defer();
  return deferred.promise
  .then(Twitter.verify().then(function () {
    console.log("in tweet verified success");

    tUrl = 'https://api.twitter.com/1.1/statuses/update.json';
    tParams = {
      'status': _message,
      'trim_user': 'true'
    };
    return Twitter.apiPostCall({
      url: tUrl,
      params: tParams
    });

  }, function (_error) {
    deferred.reject(JSON.parse(_error.text));
    console.log("in tweet " + _error.text);
  })
  );
},
// uses oAuth library to make a GET call
// @param _options.url
// @param _options.params
apiGetCall: function (_options) {
  var deferred = $q.defer();

// javascript OAuth will care of else for app we need to send only the options
oauth = oauth || OAuth(options);

var _reqOptions = angular.extend({}, _options);
_reqOptions = angular.extend(_reqOptions, {
  success: function (data) {
    deferred.resolve(JSON.parse(data.text));
  },
  failure: function (error) {
    deferred.reject(JSON.parse(error.text));
  }
});

oauth.request(_reqOptions);
return deferred.promise;
},
//uses oAuth library to make a POST call
//@param _options.url
//@param _options.params
apiPostCall: function (_options) {
  var deferred = $q.defer();

  oauth = oauth || OAuth(options);

  oauth.post(_options.url, _options.params,
    function (data) {
      deferred.resolve(JSON.parse(data.text));
    },
    function (error) {
      console.log("in apiPostCall reject " + error.text);
      deferred.reject(JSON.parse(error.text));
    }
    );
  return deferred.promise;
},
//
//clear out the tokens stored in local storage
logOut: function () {
// alert('TEST logOut');
$rootScope.data = undefined;
window.localStorage.removeItem(twitterKey);
options.accessTokenKey = null;
options.accessTokenSecret = null;
// console.log("Please authenticate to use this app");
// $state.go('login');
}
};
return Twitter;
})

app.constant('myAppConfig', {
  oauthSettings: {
    consumerKey: '22F8Eu0qVYPm14JWnwejJkDLX',
    consumerSecret: 'UsD0c6F4XnEDeoLvOpBrfZM02s0OSM4lkYS3rGDm2ZqEfT91KF',
    requestTokenUrl: 'https://api.twitter.com/oauth/request_token',
    authorizationUrl: "https://api.twitter.com/oauth/authorize",
    accessTokenUrl: "https://api.twitter.com/oauth/access_token",
    callbackUrl: "callbackUrl"
  }
});
