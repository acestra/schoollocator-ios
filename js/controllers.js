app.controller('HomeCtrl', function($scope){ 
school_skip_count=0;
news_skip_count=0;
});

app.controller('FindCtrl', function($scope,$state,ajax,area,api,UserService,location,$ionicLoading,$cordovaDialogs,ClockSrv) {
  angular.element(document.querySelector("#keyboardhide")).removeClass("scroll-content");
  location.set();
  $scope.choice = 'name';
  $scope.test=UserService.getUser();
  school_skip_count=0;
  news_skip_count=0;
  $scope.search = function(schoolname,pincode) {
    if(!schoolname){
      var data={'SchoolName':'','Pincode':pincode};
    }else if(!pincode){
      var data={'SchoolName':schoolname,'Pincode':''};
    }
    $state.go('tab.find-list',{obj:data});
  }
  ClockSrv.startClock(function(){
    location.set();
  });   
})

app.controller('FindListCtrl', function($scope,ajax,area,$state,api,location,$stateParams,$ionicScrollDelegate,$cordovaDialogs) {
  location.set();
  $scope.url=api.url; 
  $scope.schools=[];
  $scope.loading = true;  
  var currentposition=area.getloc();
  if(currentposition==null)
  var currentposition={lat:0,lng:0};
  var searchkey = $stateParams.obj;
  $ionicScrollDelegate.scrollTop();
  $scope.new_skip_counts=0;
  var objdata={ auth_token : auth_token,pincode:searchkey.Pincode,schoolname:searchkey.SchoolName,latitude:currentposition.lat,longitude:currentposition.lng,skip_limit:skip_limit,new_skip_count:school_skip_count};
  ajax.post(api.url+'get_school_list',objdata).then(function(result) {     
      if( result.status =="success" ){    
      $scope.loading = false;   
      $scope.schools=result.data; 
      school_skip_count = school_skip_count + result.skip_limit;
        if(school_skip_count!=0){
          $scope.new_skip_counts=school_skip_count;
        }else{
          $scope.new_skip_counts=0;
        }
      }else if(result.status =="error"){
        $scope.loading = false;          
        $cordovaDialogs.alert(result.message, 'School Finder', 'OK').then(function() {
           $state.go('tab.find',{});
        });
      }
    },function(error){
      if(error == 500 || error==404 || error == 0){
        $cordovaDialogs.alert('No (or low) Internet connection. Please check your settings. ,please try after sometime !!', 'School Finder', 'OK').then(function() {});
      }
  }); 
  $scope.detail = function(value) {
    console.log(value);
    $state.go('tab.find-detail',{obj:value});
  };
  $scope.core = function (core) {
    if(core == "CBSE")
      return "cbse"
    else if(core == "ICSE")
      return "icse"
    else if(core == "STATE BOARD")
      return "state"
    else if(core == "MATRIC")
      return "matric"
    else if(core == "PLAY SCHOOL")
      return "play"
    else if(core == "KINDER GARDEN")
      return "kinder"
    else 
      return "cbse"
  }
  $scope.mymap=function(datavalue){ 
    $state.go('tab.find-list-map',{obj:datavalue});
  }

$scope.moredata = false;
$scope.load=function(){
  $scope.category_id = localStorage.getItem("category_id");
  $scope.infiniteloading=true;
  var obj={ auth_token : auth_token,pincode:searchkey.Pincode,schoolname:searchkey.SchoolName,latitude:currentposition.lat,longitude:currentposition.lng,skip_limit:skip_limit,new_skip_count:$scope.new_skip_counts};
  console.log(obj);
  ajax.post(api.url+'get_school_list',obj).then(function(result) {        
   if( result.status =="success" ){     
        school_skip_count = school_skip_count + result.skip_limit;
        if(school_skip_count!=0){
          $scope.new_skip_counts=school_skip_count;
        }else{
          $scope.new_skip_counts=0;
        }
        angular.forEach(result.data,function(item){
          $scope.schools.push(item);
        })
        if ($scope.schools.length > 1000 ) {
          $scope.moredata = true;
          $ionicHistory.clearCache();
        }      
        $scope.infiniteloading=false;     
        $scope.$broadcast('scroll.infiniteScrollComplete'); 
    }else if(result.status =="error"){      
      $scope.infiniteloading=false; 
      $cordovaDialogs.alert(result.message, 'School Finder', 'OK').then(function() {});
       angular.element(document.querySelector(".dynamicheight")).removeClass("infiniteloading");
    }else{
      $scope.infiniteloading=false;       
      angular.element(document.querySelector(".dynamicheight")).removeClass("infiniteloading");
    }
  });
};
})

app.controller('FindListMapCtrl', function($scope,location,$cordovaDialogs,$stateParams,$state,area,ajax,api,$ionicLoading,$ionicHistory) {
  location.set();
  $scope.url=api.url;
  var obje=$stateParams.obj;
  initialize(obje);    
  function initialize(obj){
    var mapOptions = {
      center: new google.maps.LatLng(obj[0].latitude,obj[0].longitude),
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var map = new google.maps.Map(document.getElementById("map"),
      mapOptions);

    var infowindow = new google.maps.InfoWindow();

    var marker, i;

    for (i = 0; i < obj.length; i++) {  
      marker = new google.maps.Marker({
        position: new google.maps.LatLng(obj[i].latitude,obj[i].longitude),
        map: map
      });

      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
          infowindow.setContent(obj[i].name);
          infowindow.open(map, marker);
          $scope.track(obj[i].latitude,obj[i].longitude);
        }
      })(marker, i));
    }

    $scope.map = map;
    $scope.xpos=obj[0].latitude;
    $scope.ypos=obj[0].longitude;
    $scope.track=function(x,y){      
      var currentposition=area.getloc();
      var mylocation= new google.maps.LatLng(currentposition.lat,currentposition.lng);
      var directionsService = new google.maps.DirectionsService();
      var directionsDisplay = new google.maps.DirectionsRenderer();
      var request = {
        origin :  new google.maps.LatLng(x,y),
        destination : mylocation,
        travelMode : google.maps.TravelMode.DRIVING
      };
      directionsService.route(request, function(response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
          directionsDisplay.setDirections(response);
        }
      });
      directionsDisplay.setMap(map); 
    }
  }
  google.maps.event.addDomListener(window, 'load', initialize);
});

app.controller('FindDetailCtrl', function($scope,$state,location,$stateParams,$ionicLoading,$cordovaDialogs,api,ajax) {
  location.set();
  $scope.url=api.url;
  $scope.selectedTab = 'details';
  $scope.route=false;
  $scope.detail=$stateParams.obj;  
  $scope.parJson = function (json) {
    if(json)
      return JSON.parse(json); 
    return null;  
  }
  $scope.reviews=false;
  $scope.check = function () {
    var user=localStorage.getItem('user');
    if(user == null || user == "" || !user){        
      $cordovaDialogs.alert('Please login,after feed your review', 'School Finder', 'OK').then(function(buttonIndex) {   
        return false;              
      });
    }else{
      return true; 
    }
  }
  $scope.reviewapply = function (value,sid) {
    this.reviews=false;
    var user=localStorage.getItem('user');
    var obj={auth_token:auth_token,user_id:user,school_id:sid,rating:value};
    $ionicLoading.show({template: '<ion-spinner icon="ripple" class="spinner-calm"></ion-spinner>'});
    ajax.post(api.url+'user_rating',obj).then(function(result) {     
      if( result.status =="success" )
      {    
        $ionicLoading.hide();
        $cordovaDialogs.alert(result.message, 'School Finder', 'OK').then(function() {});    
      }else if(result.status =="error"){
        $ionicLoading.hide();
        $cordovaDialogs.alert(result.message, 'School Finder', 'OK').then(function() {});     
      }
    },function(error){
      $ionicLoading.hide(); 
      if(error == 500 || error==404 || error == 0){
        $cordovaDialogs.alert('No (or low) Internet connection. Please check your settings. ,please try after sometime !!', 'School Finder', 'OK').then(function() {});
      }
    });
  }
  $scope.core = function (core) {
    if(core == "CBSE")
      return "cbse"
    else if(core == "ICSE")
      return "icse"
    else if(core == "STATE BOARD")
      return "state"
    else if(core == "MATRIC")
      return "matric"
    else if(core == "PLAY SCHOOL")
      return "play"
    else if(core == "KINDER GARDEN")
      return "kinder"
    else 
      return "cbse"
  }
  var temp=[];
  $scope.myobj=[];
  $scope.detailmap=function(data){ 
    temp=$scope.myobj.push(data);
    $state.go('tab.find-list-map',{obj:[data]});
  }
});

app.controller('SchoolCtrl', function($scope,$state,ajax,api,$ionicScrollDelegate,$cordovaDialogs) {
  $scope.shows=false;
  $scope.url=api.url;
  $scope.loading = true;  
  $ionicScrollDelegate.scrollTop();
  $scope.new_skip_counts=0;
  var obj={ auth_token : auth_token,skip_limit:skip_limit,new_skip_count:news_skip_count}
  ajax.post(api.url+'get_school_news',obj).then(function(result) {    
      if( result.status =="success" ){    
      $scope.loading = false;   
        $scope.schoolnews=result.data;      
        news_skip_count = news_skip_count + result.skip_limit;
         if(news_skip_count!=0 ){
          $scope.new_skip_counts=news_skip_count;
        }else{
          $scope.new_skip_counts=0;
        }
      }else if(result.status =="error"){
        $scope.loading = false;          
        $cordovaDialogs.alert(result.message, 'School Finder', 'OK').then(function() {
           $state.go('tab.find',{});
        });
      }
    },function(error){
      if(error == 500 || error==404 || error == 0){
        $cordovaDialogs.alert('No (or low) Internet connection. Please check your settings. ,please try after sometime !!', 'School Finder', 'OK').then(function() {});
      }
    }); 
  $scope.news = function(value){
    $state.go('tab.school-detail',{obj:value});
  }; 
  var temp=JSON.stringify($scope.schoolnews);
  if(temp == '{}' || temp == '[]'){
    $scope.shows=true;
  }
  $scope.moredata = false;
  $scope.load=function(){
  $scope.category_id = localStorage.getItem("category_id");
  $scope.infiniteloading=true;
  var obj={ auth_token : auth_token,skip_limit:skip_limit,new_skip_count:$scope.new_skip_counts}
  ajax.post(api.url+'get_school_news',obj).then(function(result) {      
   if( result.status =="success" ){     
        news_skip_count = news_skip_count + result.skip_limit;
        if(news_skip_count!=0){
          $scope.new_skip_counts=news_skip_count;
        }else{
          $scope.new_skip_counts=0;
        }
        angular.forEach(result.data,function(item){
          $scope.schoolnews.push(item);
        })
        if ($scope.schoolnews.length > 500 ) {
          $scope.moredata = true;
          $ionicHistory.clearCache();
        } 
        $scope.infiniteloading=false;           
        $scope.$broadcast('scroll.infiniteScrollComplete');
   }else if(result.status =="error"){      
      $scope.infiniteloading=false; 
      $cordovaDialogs.alert(result.message, 'School Finder', 'OK').then(function() {});
       angular.element(document.querySelector(".dynamicheight")).removeClass("infiniteloading");
    }else{
      $scope.infiniteloading=false;       
      angular.element(document.querySelector(".dynamicheight")).removeClass("infiniteloading");
    }
  });
};
});

app.controller('SchoolDetailCtrl', function($scope, $stateParams,api) {
  $scope.detail=$stateParams.obj; 
  $scope.url=api.url;
});

app.controller('RatingCtrl', function($scope, $state, $q, UserService, $ionicLoading,$cordovaDialogs,ajax,api,TwitterLib,$ionicActionSheet) {
  var user=localStorage.getItem("user");
  $scope.demoreview=false;
  $scope.fbin=false;$scope.twtin=false;
  $scope.fbout=false;$scope.twout=false;
  if(user == null || user == "" || !user){        
    $scope.fbin=true;$scope.twtin=true;
    $scope.fbout=false;$scope.twout=false;
    $scope.demoreview=false;
  }else{
    $scope.demoreview=true;
    var ltype=localStorage.getItem("logintype");
    if(ltype=='facebook'){
      $scope.fbin=false;$scope.twtin=false;
      $scope.fbout=true;$scope.twtout=false;
    }else if(ltype=='twitter'){
      $scope.fbin=false;$scope.twtin=false;
      $scope.twtout=true;$scope.fbout=false;
    }    
  }

// This is the success callback from the login method
var fbLoginSuccess = function(response) {
  if (!response.authResponse){
    fbLoginError("Cannot find the authResponse");
    return;
  }

  var authResponse = response.authResponse;

  getFacebookProfileInfo(authResponse)
  .then(function(profileInfo) {
    $scope.demoreview=true;
    $scope.fbin=false;$scope.twtin=false;
    $scope.fbout=true;$scope.twtout=false;
    localStorage.setItem("logintype","facebook");
// For the purpose of this example I will store user data on local storage
UserService.setUser({
  authResponse: authResponse,
  userID: profileInfo.id,
  name: profileInfo.name,
  email: profileInfo.email,
  picture : "http://graph.facebook.com/" + authResponse.userID + "/picture?type=large"
});
localStorage.setItem("user",profileInfo.email);
var obj={ auth_token : auth_token,logintype:"facebook",username:profileInfo.name,email:profileInfo.email};         
ajax.post(api.url+'user_credential',obj).then(function(result) {     
  if( result.status =="success" ){
    localStorage.setItem("user",result.userid);
  }else if(result.status =="error"){
    $cordovaDialogs.alert(result.message, 'School Finder', 'OK').then(function() {});
  }
},function(error){
  if(error == 500 || error==404 || error == 0){
    $cordovaDialogs.alert('No (or low) Internet connection. Please check your settings. ,please try after sometime !!', 'School Finder', 'OK').then(function() {});
  }
});
// $state.go('app.home');
}, function(fail){
// Fail get profile info
console.log('profile info fail', fail);
});
};

// This is the fail callback from the login method
var fbLoginError = function(error){
  console.log('fbLoginError', error);
  $ionicLoading.hide();
};

// This method is to get the user profile info from the facebook api
var getFacebookProfileInfo = function (authResponse) {
  var info = $q.defer();

  facebookConnectPlugin.api('/me?fields=email,name&access_token=' + authResponse.accessToken, null,
    function (response) {
      console.log(response);
      info.resolve(response);
    },
    function (response) {
      console.log(response);
      info.reject(response);
    }
    );
  return info.promise;
};

//This method is executed when the user press the "Login with facebook" button
$scope.facebookSignIn = function() {
  facebookConnectPlugin.getLoginStatus(function(success){
    if(success.status === 'connected'){
// The user is logged in and has authenticated your app, and response.authResponse supplies
// the user's ID, a valid access token, a signed request, and the time the access token
// and signed request each expire
console.log('getLoginStatus', success.status);

// Check if we have our user saved
var user = UserService.getUser('facebook');

if(!user.userID){
  getFacebookProfileInfo(success.authResponse)
  .then(function(profileInfo) {
    localStorage.setItem("logintype","facebook");
    $scope.demoreview=true;
    $scope.fbin=false;$scope.twtin=false;
    $scope.fbout=true;$scope.twtout=false;
// For the purpose of this example I will store user data on local storage
UserService.setUser({
  authResponse: success.authResponse,
  userID: profileInfo.id,
  name: profileInfo.name,
  email: profileInfo.email,
  picture : "http://graph.facebook.com/" + success.authResponse.userID + "/picture?type=large"
});
localStorage.setItem("user",profileInfo.email);
var obj={ auth_token : auth_token,logintype:"facebook",username:profileInfo.name,email:profileInfo.email};         
ajax.post(api.url+'user_credential',obj).then(function(result) {     
  if( result.status =="success" ){
    localStorage.setItem("user",result.userid);
  }else if(result.status =="error"){
    $cordovaDialogs.alert(result.message, 'School Finder', 'OK').then(function() {});
  }
},function(error){
  if(error == 500 || error==404 || error == 0){
    $cordovaDialogs.alert('No (or low) Internet connection. Please check your settings. ,please try after sometime !!', 'School Finder', 'OK').then(function() {});
  }
});
$scope.demoreview=true;
$scope.fbin=false;$scope.twtin=false;
$scope.fbout=true;$scope.twtout=false;
// $state.go('app.home');
}, function(fail){
// Fail get profile info
console.log('profile info fail', fail);
});
}else{
  $scope.demoreview=true;
  $scope.fbin=false;$scope.twtin=false;
  $scope.fbout=true;$scope.twtout=false;
// $state.go('app.home');
}
} else {
// If (success.status === 'not_authorized') the user is logged in to Facebook,
// but has not authenticated your app
// Else the person is not logged into Facebook,
// so we're not sure if they are logged into this app or not.

console.log('getLoginStatus', success.status);

// $ionicLoading.show({
//   template: 'Logging in...'
// });

// Ask the permissions you need. You can learn more about
// FB permissions here: https://developers.facebook.com/docs/facebook-login/permissions/v2.4
facebookConnectPlugin.login(['email', 'public_profile'], fbLoginSuccess, fbLoginError);
}
});
};

$scope.facebookSignOut = function() {
  var hideSheet = $ionicActionSheet.show({
    destructiveText: 'Logout',
    titleText: 'Are you sure you want to logout? This app is awsome so I recommend you to stay.',
    cancelText: 'Cancel',
    cancel: function() { hideSheet();},
    buttonClicked: function(index) {
      return true;
    },
    destructiveButtonClicked: function(){
      $ionicLoading.show({
        template: 'Logging out...'
      });
// Facebook logout
facebookConnectPlugin.logout(function(){
  $ionicLoading.hide();
  localStorage.removeItem("user");
  $scope.demoreview=false;
  $scope.fbin=true;$scope.twtin=true;
  $scope.fbout=false;$scope.twtout=false;
  localStorage.removeItem("starter_facebook_user");
  hideSheet();
  return true;
// $state.go('welcome');
},
function(fail){
  $ionicLoading.hide();
});
}
});       
};
$scope.twitterSignIn = function () {
  TwitterLib.init().then(function (_data) {
    $scope.demoreview=true;
    $scope.fbin=false;$scope.twtin=false;
    $scope.fbout=false;$scope.twtout=true;
    var obj={ auth_token : auth_token,logintype:"twitter",username:_data.name,email:_data.email};         
    ajax.post(api.url+'user_credential',obj).then(function(result) {     
      if( result.status =="success" ){
        localStorage.setItem("user",result.userid);
        localStorage.setItem("logintype","twitter");
      }else if(result.status =="error"){
        $cordovaDialogs.alert(result.message, 'School Finder', 'OK').then(function() {});
      }
    },function(error){
      $ionicLoading.hide(); 
      if(error == 500 || error==404 || error == 0){
        $cordovaDialogs.alert('No (or low) Internet connection. Please check your settings. ,please try after sometime !!', 'School Finder', 'OK').then(function() {});
      }
    });

  }, function error(_error) {
    $cordovaDialogs.alert(_error,'School Finder','ok').then(function() {});
  });
};
$scope.twitterSignOut = function () {
  localStorage.removeItem("user");
  $scope.demoreview=false;
  $scope.fbin=true;$scope.twtin=true;
  $scope.fbout=false;$scope.twtout=false;
  TwitterLib.logOut();
};
});